class regles ():
    def __init__(self, amorce, apartirde, prefixe, nomfichier, postfixe, extension):
        """
        Définition du constructeur
        """
        self.amorce = amorce
        self.apartirde = apartirde
        self.prefixe = prefixe
        self.nomfichier = nomfichier
        self.postfixe = postfixe
        self.extension = extension
    
    def __str___(self):
        return "{} {} {} {} {} {}".format(self.amorce, self.apartirde, self.prefixe, self.nomfichier, self.postfixe, self.extension)
    
    def getAmorce(self):
        """
        Récupération de l'amorce
        """
        return self.amorce
        
    
    def getApartirde(self):
        """
        Récupération de l'apartirde rentré par l'utilisateur
        """
        return self.apartirde
    
    def getPrefixe(self):
        """
        Récupération du préfixe
        """
        return self.prefixe
    
    def getNomFichier(self, varfichier):
        """
        Contrôle et récupération du nom du fichier
        """
        if self.nomfichier is None:
            return False
        elif self.nomfichier == varfichier:
            return True
        else:
            self.nomfichier = varfichier
            return self.nomfichier
    
    def getPostfixe(self):
        """
        Récupération du sufixe
        """
        return self.postfixe
    
    def getExtension(self):
        """
        Récupération de l'extension du fichier
        """
        return self.extension
    
help(regles)
    
        
        
        
    
        