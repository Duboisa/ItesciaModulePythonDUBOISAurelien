from ExerciceRenommageRegles import *
class action():
    def __init__(self, nomdurepertoire, regle):
        """
        Définition du constructeur
        """
        self.nomdurepertoire = nomdurepertoire
        self.regle = regle
    
    def getNomRepertoire(self):
        """
        Récupération du répertoire spécifié
        """
        return self.nomdurepertoire
    
    def getRegles(self):
        return self.regle
    
    def setSimule(self):
        """
        Simulation du renommage
        """
        if regles.getNomFichier() is False:
            return "Error"
    
help(action)

        