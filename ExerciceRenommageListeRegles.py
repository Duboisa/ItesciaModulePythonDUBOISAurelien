from ExerciceRenommageRegles import *
class listregles():
    def __init__(self, regles):
        self.regles = regles
    
    def __str__(self):
        return "{}".format(self.regles)
    
    def getRegles(self):
        """
        Récupération des règles de renommage
        """
        return self.regles
    
    def setSaveRegles(self):
        """
        Sauvegarde d'une règle dans un fichier "NOMLOGICIEL.ini"
        """
        with open("NOMLOGICIEL.ini", 'w') as fichier:
            for i in self.regles:
                fichier.write(i)     
    
    def setLoadRegles(self):
        """
        Loading d'une règle à partir du fichier "NOMFICHIER.ini"
        """
        self.regles = []
        with open("NOMLOGICIEL.ini", 'r') as fichier:
            for line in fichier:
                line=line.split()
                self.regles.append(line)
        return self.regles

help(listregles)